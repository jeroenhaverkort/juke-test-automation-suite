package userInterfaceTests;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class SmokeTests extends JukeTest {

    @Test
    public void openWebsiteTest(){

        // Get the window title from the browser
        String actualWindowTitle = driver.getTitle();

        // AssertJ assertion:
        Assertions.assertThat(actualWindowTitle).as("Title was not correct").isEqualTo("Home | JUKE.nl");

        // JUnit assertion:
//        Assert.assertEquals("Title was not correct", "Home | JUKE.nl", actualWindowTitle);

    }
}
