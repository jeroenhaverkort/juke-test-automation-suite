package userInterfaceTests;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class StationTests extends JukeTest {

    @Test
    public void stationReader() {

        driver.get("https://radio.juke.nl/radiozenders");

        List<WebElement> allItems = driver.findElements(By.xpath("//button[@data-automation-id='play']"));

        allItems.remove(allItems.size() - 1); // The last item is not displayed on the site

        List<String> allStationsList = new ArrayList<>();

        for (WebElement item : allItems) {
            item.click();

            WebElement element = driver.findElement(By.xpath("//header/h2"));
            allStationsList.add(element.getText());

            if (driver.findElements(By.xpath("//button[@data-automation-id='add_track']")).size() == 0) {
                System.out.println("WARNING!!! " + element.getText() + " has NO playlist");
            }
        }

        System.out.println(allStationsList);
        System.out.println(allStationsList.size());


    }

    @Test
    public void stationCollectionReader() throws InterruptedException {

        // Get list from Juke FM website
        Object allCollectionsList = generateListFromJukeFM(
                "https://radio.juke.nl/radiozenders",
                "//div/h2");

        //Get list from Contentful
        Object allContentfulCollectionsList = generateListFromContentful(
                "https://app.contentful.com/spaces/r4m7wjlnqcgy/entries/3K3csbyWUwOuyaOKQsEwmM",
                "//div[@class='ReferenceCard__title']");

        //Compare both lists from contentful and Juke FM
        compareLists((List<String>) allContentfulCollectionsList, (List<String>) allCollectionsList);
    }

    @Test
    public void allStationsReader() throws InterruptedException {

        Object allCollectionsList = generateListFromModalJukeFM(
                "https://radio.juke.nl/",
                "//aside//div[contains(@data-automation-id,'radio_')]",
                "//div[@data-automation-id='Radiozenders']/div/button");


        Object allContentfulCollectionsList = generateListFromContentful(
                "https://app.contentful.com/spaces/r4m7wjlnqcgy/entries/1o4uE4I07aU82kgoWO2qyy",
                "//div[@class='ReferenceCard__title']");

        List<String> list1 = (List<String>) allContentfulCollectionsList;
        List<String> list2 = (List<String>) allCollectionsList;

        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i).equals(list2.get(i))) {

            } else {
                System.out.println(list1.get(i) + " != " + list2.get(i));
            }
        }
    }

    private Object generateListFromModalJukeFM(String webLocation, String xPathLocator, String modalOpener) {
        driver.get(webLocation);
        wait.until(elementToBeClickable(By.xpath(modalOpener))).click();

        List<WebElement> allCollections = driver.findElements(By.xpath(xPathLocator));
        List<String> allCollectionsList = new ArrayList<>();

        for (WebElement collection : allCollections) {
            String object = collection.getAttribute("data-automation-id");
            String editedObject = object.replace('-', ' ').toLowerCase().substring(6);
            allCollectionsList.add(editedObject);
        }
        System.out.println("Order Juke FM    : " + allCollectionsList);
        System.out.println(allCollectionsList.size());


        return allCollectionsList;
    }

    private Object generateListFromJukeFM(String webLocation, String xPathLocator) {
        driver.get(webLocation);

        List<WebElement> allCollections = driver.findElements(By.xpath(xPathLocator));
        List<String> allCollectionsList = new ArrayList<>();
        for (WebElement collection : allCollections) {
            allCollectionsList.add(collection.getText().toLowerCase());
        }
        System.out.println("Order Juke FM    : " + allCollectionsList);
        return allCollectionsList;

    }

    private Object generateListFromContentful(String webLocation, String xPathLocator) throws InterruptedException {

        driver.get(webLocation);
        wait.until(elementToBeClickable(By.xpath("//input[@name='user[email]']"))).sendKeys("<CONTENTFUL USERNAME");
        driver.findElement(By.xpath("//input[@name='user[password]']")).sendKeys("CONTENTFUL PASSWORD");
        driver.findElement(By.xpath("//input[@value='Log in']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathLocator)));
        Thread.sleep(3000);
        List<WebElement> allContentfulCollections = driver.findElements(By.xpath(xPathLocator));
        List<String> allContentfulCollectionsList = new ArrayList<>();
        Thread.sleep(3000);
        for (WebElement collection : allContentfulCollections) {
            allContentfulCollectionsList.add(collection.getText().toLowerCase());
        }
        Thread.sleep(1000);
        System.out.println("Order Contentful : " + allContentfulCollectionsList);
        return allContentfulCollectionsList;
    }

    private void compareLists(List<String> baseList, List<String> frontendList) {

        int j = 0;
        boolean k = true;
        for (int i = 0; i < baseList.size(); i++) {
            if (baseList.get(i).equals(frontendList.get(j))) {
                j++;
                if (i == baseList.size()-1 && k) {
                    System.out.println("all lists are the same");
                }
            } else {
                System.out.println(baseList.get(i) + " is missing in collection list");
                k = false;
            }
        }
    }
}
